<?php

/**
 * @file
 * Contains the references_dialog_search_api_plugin_display display plugin.
 */

/**
 * A plugin to handle references_dialog with search_api on views.
 *
 * @ingroup views_display_plugins
 */
class references_dialog_search_api_plugin_display extends references_dialog_plugin_display {

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    if ($form_state['section'] == 'attach') {
      $entity_type = references_dialog_search_api_get_entity_type($this->view);
      $attachables = references_dialog_get_search_view_attachables($entity_type);
      $options = array();
      foreach ($attachables as $name => $attachable) {
        $options[$name] = $attachable['label'];
      }
      $form['attach'] = array(
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => $this->get_option('attach'),
        '#title' => t('Attach to'),
        '#description' => t('Choose what to attach this view to.')
      );
    }
  }

  function execute_hook_menu() {
    $items = array();
    $attachables = $this->get_option('attach');
    $view_name = $this->view->name;
    $display_name = $this->display->id;
    foreach ($attachables as $attachable) {
      $items['references-dialog/search/' . $view_name . '/' . $display_name . '/' . $attachable] = array(
        'type' => MENU_CALLBACK,
        'page callback' => 'references_dialog_search_api_search_view',
        'page arguments' => array($view_name, $display_name, $attachable),
        'access callback' => 'views_access',
      );
    }
    $this->_set_access_arguments($items);

    return $items;
  }

  function render() {
    $output = theme($this->theme_functions(), array('view' => $this->view));
    // Add the data necessary to javascript.
    $js_result = array();
    $entity_type = references_dialog_search_api_get_entity_type($this->view);
    $entity_info = entity_get_info($entity_type);
    $entity_key = $entity_info['entity keys']['id'];

    $entity_ids = array();
    if (strpos($this->view->base_table, 'search_api_index_') !== FALSE) {
      // Make sure we have the indexed entity key added.
      if (isset($this->view->result[0]->_entity_properties[$entity_key])) {
        foreach ($this->view->result as $row => $result) {
          $entity_ids[] = $result->_entity_properties[$entity_key];
        }
      }
    }
    else {
      foreach ($this->view->result as $row => $result) {
        $entity_ids[] = $result->{$this->view->base_field};
      }
    }

    $entities = entity_load($entity_type, $entity_ids);

    if (strpos($this->view->base_table, 'search_api_index_') !== FALSE) {
      foreach ($this->view->result as $result) {
        $js_result[] = array(
          'entity_id' => isset($result->_entity_properties[$entity_key]) ? $result->_entity_properties[$entity_key] : null,
          'title' => entity_label($entity_type, $entities[$result->_entity_properties[$entity_key]]),
          'entity_type' => $entity_type,
        );
      }
    }
    else {
      foreach ($this->view->result as $result) {
        $js_result[] = array(
          'entity_id' => $result->{$this->view->base_field},
          'title' => entity_label($entity_type, $entities[$result->{$this->view->base_field}]),
          'entity_type' => $entity_type,
        );
      }
    }
    drupal_add_js(drupal_get_path('module', 'references_dialog') . '/js/search-reference.js');
    drupal_add_js(array('ReferencesDialog' => array('entities' => $js_result)), 'setting');

    return $output;
  }

}
